A.  Create your subgroup and project by logging into GitLab using the web link provided and using the “GitLab How-To” web link, and do the following:



B.  Create a README file that includes notes describing where in the code to find the changes you made for each of parts C to J. Each note should include the prompt, file name, line number, and change.



C.  Customize the HTML user interface for your customer’s application. The user interface should include the shop name, the product names, and the names of the parts.


Line 14 in mainscreen.html was changed to <title>Phone Shop</title> -- this changed the title name 
Line 16 in mainscreen.html was changed to <body style="background-color: #fcd4db;"></body> -- this changed the color of the background 
Line 19 in mainscreen.html was changed to <h1>Phone Shop</h1> -- this changed the main name of the shop 
Line 21 in mainscreen.html was changed to <h2>Phone Parts</h2> -- this changed the the name of the parts section to Phone parts
Line 30 in mainscreen.html was changed to <a th:href="@{/showFormAddInPart}" class="btn btn-primary btn-sm mb-3">Add Inhouse Phone Part</a> -- this changed the name of the inhouse part section
Line 31 in mainscreen.html was changed to <a th:href="@{/showFormAddOutPart}" class="btn btn-primary btn-sm mb-3">Add Outsourced Phone Part</a> -- this changed the name of the outsourced part section
Line 53 in mainscreen.html was changed to <h2>Phone Products</h2> -- this changed the name of the product section 


D.  Add an “About” page to the application to describe your chosen customer’s company to web viewers and include navigation to and from the “About” page and the main screen.



E.  Add a sample inventory appropriate for your chosen store to the application. You should have five parts and five products in your sample inventory and should not overwrite existing data in the database.



F.  Add a “Buy Now” button to your product list. Your “Buy Now” button must meet each of the following parameters:



G. Modify the parts to track maximum and minimum inventory by doing the following:



H. Add validation for between or at the maximum and minimum fields. The validation must include the following:



I.  Add at least two unit tests for the maximum and minimum fields to the PartTest class in the test package.


J.  Remove the class files for any unused validators in order to clean your code.


K.  Export your project from the IDE as a ZIP file.



L.  Demonstrate professional communication in the content and presentation of your submission.


